// database config
//require('./config/db_config');
var config = require('./config');

// variables
var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var session = require('express-session');

var tbRoutes = require('./routes/textbook');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// initialize app session
app.use(session({ 
    secret: config.cookieSecret,
    cookie: { maxAge: 3600000 },
    resave: true,
    saveUninitialized: true
}));

// load passport config files
require('./passport')(passport);

// initialize passport session
app.use(passport.initialize());
app.use(passport.session());

// main page route
app.get('/', function (req, res) { res.redirect('/textbooks'); });

// fb auth routes
var authRoutes = require('./routes/auth')(app, passport);

// textbook routes
app.get('/textbooks', tbRoutes.list);
app.post('/textbooks', tbRoutes.sort);
app.post('/textbooks/add', ensureAuthenticated, tbRoutes.add);
app.post('/textbooks/search', tbRoutes.search);
app.post('/textbooks/:id/delete', tbRoutes.delete);

function ensureAuthenticated(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    else {
        res.send({ error: 'user is not logged in' });
    }
}

/// catch 404 and forward to error handler
/*app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});*/

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

mongoose.connect(config.db.session);

var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});

module.exports = app;
