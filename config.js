// load modules
var mongoose = require('mongoose');

// all config stuff in here
var config = {};

if (process.env.NODE_ENV === 'development') {
	config.cookieSecret = '2fW118947J5O82Qk6FIdE1pXtey27goB';
	config.db = {
		session: 'mongodb://localhost/textbooks'
	}
	config.appId = '1472368536339594';
	config.appSecret = '61ef92ece682c8a68ed13036ed390419';
	config.callbackURL = 'http://localhost:3000/auth/facebook/callback'; 
}

else if (process.env.NODE_ENV === 'production') {
	config.cookieSecret = 'mfg4ND21Dt95KJxeBNj847W099RasBLz';
	config.db = {
		session: 'mongodb://heroku_app28895800:v2cp5tb7r7ism3bmv0up4mi8hf@ds063919.mongolab.com:63919/heroku_app28895800'
	}
	config.appId = '1472158093027305';
	config.appSecret = 'c6f0c84bccef8f72d7b27c59a93a4340';
	config.callbackURL = 'http://uw-textbook-exchange.herokuapp.com/auth/facebook/callback'
}


module.exports = config;