// dependencies
var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

// textbook schema
var tbSchema = new Schema({
	courseCode: { type: String },
	bookTitle: { type: String },
	addedBy: { type: ObjectId, ref: 'User' },
	addedOn: { type: String },
	buysell: String,
	expires: { type: Date, expires: 60 * 60 * 24 * 7, default: Date.now }				// UNCOMMENT THIS FOR PROD
});

tbSchema.index({ courseCode: 1, _id: 1 });

// validation middleware
tbSchema.pre('save', function(next) {
	var courseCode = this.courseCode;
	var bookTitle = this.bookTitle;
	var addedOn = this.addedOn;

	// checking for required fields for books
	if (!courseCode || !bookTitle || !addedOn) {
		return next(new Error('one or more required fields empty'));
	}

	// valid!
	next();
});

// export
module.exports = mongoose.model('Textbook', tbSchema);