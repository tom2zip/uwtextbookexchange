// dependencies
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// user schema
var userSchema = new Schema({
	name: String,
	token: String,
	id: String
});

userSchema.index({ name: 1 });

// export
module.exports = mongoose.model('User', userSchema);