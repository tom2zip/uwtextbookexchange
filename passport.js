// load modules
var config = require('./config');
var FacebookStrategy = require('passport-facebook').Strategy;

// load the user model
var User = require('./models/user');

module.exports = function(passport) {

	passport.serializeUser(function(user, done) {
	    done(null, user.id);
	});

	passport.deserializeUser(function(id, done) {
	    User.findOne()
	    	.where('id', id)
	    	.exec(function(err, user) {
	    		if(err) return err;
	    		done(null, user);
	    	})
	});

	passport.use(new FacebookStrategy({
	    
	    clientID: config.appId,
	    clientSecret: config.appSecret,
	    callbackURL: config.callbackURL

	}, function(accessToken, refreshToken, profile, done) {
	    
	    process.nextTick(function() {
	        User.findOne({ 'id': profile.id }, function(err, user) {
	        	
	            if(err) { return done(err); }
	            
	            if(user) {
	                return done(null, user);
	            }
	            else {
	                var newUser = new User({
	                    name: profile.displayName,
	                    token: accessToken,
	                    id: profile.id
	                });

	                newUser.save(function(err) {
		                if (err) throw err;
		                return done(null, newUser);
		            });
	            }

	        });
	    });

	}));
}