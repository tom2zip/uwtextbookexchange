$(document).ready(function() {
	$('#addBookForm').validate({
		rules: {
			courseName: { required: true },
			bookTitle: { required: true },
			buysell: { required: true }
		}
	});

	$('#searchCodeForm').validate({
		rules: {
			courseName: { required: true }
		}
	});

	$('#searchTitleForm').validate({
		rules: {
			bookTitle: { required: true }
		}
	});
});