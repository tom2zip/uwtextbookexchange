function removeFbHash() {
	if (window.location.hash && window.location.hash === '#_=_') {

		if (window.history && window.replaceState) {
			window.history.replaceState("", document.title, window.location.pathname);
 		 } 

 		 else {
		    // Prevent scrolling by storing the page's current scroll offset
		    var scroll = {
		    	top: document.body.scrollTop,
		    	left: document.body.scrollLeft
		    };

		    window.location.hash = "";
		    
		    // Restore the scroll offset, should be flicker free
		    document.body.scrollTop = scroll.top;
		    document.body.scrollLeft = scroll.left;
  		}
	}	
}