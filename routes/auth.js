// load models
var Textbook = require('../models/textbook');

module.exports = function(app, passport) {

	// fb authentication
	app.get('/auth/facebook', notLoggedIn, passport.authenticate('facebook', {
		scope: 'public_profile'
	}));

	// fb authentication callback
	app.get('/auth/facebook/callback', passport.authenticate('facebook', {
		scope: 'public_profile'
	}), function(req, res) {
	    console.log('authentication successful');
	    res.redirect('/');
	});

	// log people out
	app.get('/logout', function(req, res) {
		if(req.isAuthenticated()) {
			req.logout();
			req.session.destroy();
			console.log('logout successful');
			res.redirect('/');
		}
		else {
			res.send({ error: 'user is not logged in' });
		}
	});

};

function notLoggedIn(req, res, next) {
	if(!req.isAuthenticated()) {
		return next();
	}
	else {
		res.send({ error: 'user is already logged in' });
	}
	res.redirect('/');
}