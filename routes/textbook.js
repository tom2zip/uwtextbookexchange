// load modules
var moment = require('moment');
var async = require('async');
var _ = require('underscore');
var sanitizer = require('html-css-sanitizer');
var courseNameAndNum = require('../public/javascripts/courseSelect');

// load models
var Textbook = require('../models/textbook');
var User = require('../models/user');
//var Comment = require('../models/comment');

// for storing all functions
var routes = {};

// lists all textbooks - sorted by course code as default
routes.list = function(req, res) {
	var user;
	var authenticated = false;
	var title = 'UW Textbook Exchange';

	if(req.isAuthenticated()) { 
		authenticated = true;
		user = req.user;
	}

	Textbook.find()
		.sort('courseCode')
		.populate('addedBy')
		.exec(function(err, books) {
			res.render('index', {
				title: title,
				books: books, 
				user: user, 
				authenticated: authenticated 
			});
		})
};

// sorts the list
routes.sort = function(req, res) {
	var user;
	var authenticated = false;
	var title = 'UW Textbook Exchange';
	var sortOrder = 'courseCode';

	if(req.isAuthenticated()) { 
		authenticated = true;
		user = req.user;
	}

	if (req.body.sort === 'Book Title') {
		sortOrder = 'bookTitle';
	}
	else if (req.body.sort === 'Date') {
		sortOrder = 'addedOn';
	}
	
	Textbook.find()
		.populate('addedBy')
		.sort(sortOrder)
		.exec(function(err, books) {
			if(err) res.send({ error: err.message });

			res.render('index', {
				title: title,
				books: books,
				user: user,
				authenticated: authenticated
			});
		});

};

// for adding textbooks
routes.add = function(req, res) {

	// check if the user selected option '----'
	if(req.body.courseName === '') {
		return res.send({ error: 'one or more required fields empty' });
	}

	var courseName = req.body.courseName;
	var courseNum = req.body.courseNum;
	var courseCode = courseName + ' ' + courseNameAndNum[courseName][courseNum];
	var bookTitle = sanitizer.smartSanitize(req.body.bookTitle);
	var now = moment().format('MMMM DD, YYYY');

	var newBook = new Textbook({
		courseCode: courseCode,
		bookTitle: bookTitle,
		addedBy: req.user,
		addedOn: now,
		buysell: req.body.buysell
	});

	newBook.save(function(err) {
		if(err) { return res.send({ error: err.message }); }
		res.redirect('/');
	});
	
};

// search by course code or title
routes.search = function(req, res) {
	var query = req.body.query.toUpperCase();
	Textbook.find()
		.populate('addedBy')
		.exec(function(err, books) {
			if(err) return res.send({ error: err.message });

			var searchResults = [];
			async.eachSeries(books, function(book, done) {
				if(book.courseCode === query || book.bookTitle.toUpperCase() === query) {
					searchResults.push(book);
				}
				done();
			}, function(err, results) {
				if(err) return res.send({ error: err.message });
				res.render('search', { searchResults: searchResults });
			});

		})
}

// delete a textbook
routes.delete = function(req, res) {
	var id = req.params.id;
	Textbook.findById(id, function(err, book) {
		if(err) return err;
		book.remove(function(err) {
			if(err) return err;
			res.redirect('/textbooks');
		});
	});
};

module.exports = routes;